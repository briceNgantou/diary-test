import React, { useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import AppContext from "./config/AppContext";
import Home from "./screen/home/Home";
import Restaurants from "./screen/restaurants/Restaurants";
import Navbar from "./components/navbar/Navbar";
import Sidebar from "./components/sidebar/Sidebar";
import SingleRestaurant from "./screen/singleRestaurant/SingleRestaurant";
import "./app.css";
import Footer from "./components/footer/Footer";

function App() {
  const [ActualData_AccessedName, setActualData_AccessedName] = useState(null);

  return (
    <BrowserRouter>
      <AppContext.Provider
        value={{
          ActualData_AccessedName,
          setActualData_AccessedName,
        }}
      >
        <div className="app-bars">
          <div className="app-sidebar">
            <Sidebar />
          </div>
          <div className="app-body">
            <Navbar />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/:name" element={<SingleRestaurant />} />
              <Route path="/my-restaurants" element={<Restaurants />} />
            </Routes>
          </div>
          <Footer />
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
