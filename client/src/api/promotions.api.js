import axios from "axios";
import BASE_URL from "../config/constant";

const getPromotions = async (micrositeName) => {
  try {
    let result = await axios
      .get(`${BASE_URL}/resdiary/all-promotions?micrositeName=${micrositeName}`)
      .then((res) => {
        return res?.data;
      });
    return result;
  } catch (error) {
    return error;
  }
};
export default getPromotions;
