import axios from "axios";
import BASE_URL from "../config/constant";

const bookingApi = async (
  visitDate,
  visitTime,
  partySize,
  firstName,
  surname,
  email,
  specialRequests,
  phone,
  phoneCountryCode,
  promotion,
  micrositeName
) => {
  const body = {
    VisitDate: visitDate,
    VisitTime: visitTime,
    ChannelCode: "ONLINE",
    SpecialRequests: specialRequests,
    PartySize: Number(partySize),
    FirstName: firstName,
    Email: email,
    Surname: surname,
    PhoneCountryCode: phoneCountryCode,
    Phone: phone,
  };
  let uri;
  if (promotion) {
    uri = `${BASE_URL}/resdiary/booking-available-promotion?promotionId=${Number(
      promotion
    )}&micrositeName=${micrositeName}`;
  } else {
    uri = `${BASE_URL}/resdiary/booking?micrositeName=${micrositeName}`;
  }

  try {
    let bookSaved = await axios.post(uri, body).then((res) => {
      return res?.data;
    });
    return bookSaved;
  } catch (error) {
    return error;
  }
};
export default bookingApi;
