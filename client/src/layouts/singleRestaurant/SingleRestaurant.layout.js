import React, { useEffect } from "react";
import { Button, Grid, Alert, Paper, Typography } from "@mui/material";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import validator from "validator";

import "./styles.css";
import { colors } from "../../config/colors";
import Loading from "../../components/Loading";
import image from "../../asset/icon.png";
import TextInput from "../../components/form/textInput/TextInput";
import bookingApi from "../../api/booking.api";
import getPromotions from "../../api/promotions.api";

export default function SingleRestaurantLayout({ Restaurant, loading, error }) {
  const [open, setOpen] = React.useState(false);
  const [scroll, setScroll] = React.useState("paper");
  const [inputError, setInputError] = React.useState(false);
  const [message, setMessage] = React.useState("");
  const [success, setSuccess] = React.useState(false);
  const [loading_api_response, setLoading_api_response] = React.useState(false);
  const [visitDate, setVisitDate] = React.useState("");
  const [visitTime, setVisitTime] = React.useState("");
  const [specialRequests, setSpecialRequests] = React.useState("");
  const [partySize, setPartySize] = React.useState("");
  const [firstName, setFirstName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [promotion, setPromotion] = React.useState("");
  const [surname, setSurname] = React.useState("");
  const [phoneCountryCode, setPhoneCountryCode] = React.useState("");
  const [phone, setPhone] = React.useState("");
  const [allPromotions, setAllPromotions] = React.useState([]);

  const handleClickOpen = (scrollType) => () => {
    setOpen(true);
    setScroll(scrollType);
  };

  const handleClose = () => {
    setOpen(false);
    setSuccess(false);
    setInputError(false);
    setMessage("");
    setVisitDate("");
    setVisitTime("");
    setSpecialRequests("");
    setPartySize("");
    setEmail("");
    setFirstName("");
    setSurname("");
    setPhoneCountryCode("");
    setPhone("");
  };

  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);

  const handleVisitDate = (e) => {
    setVisitDate(e.target.value);
  };
  const handleVisitTime = (e) => {
    setVisitTime(e.target.value);
  };
  const handleSpecialRequests = (e) => {
    setSpecialRequests(e.target.value);
  };
  const handlePartySize = (e) => {
    setPartySize(e.target.value);
  };
  const handleFirstName = (e) => {
    setFirstName(e.target.value);
  };
  const handleEmail = (e) => {
    setEmail(e.target.value);
  };
  const handleSurname = (e) => {
    setSurname(e.target.value);
  };
  const handlePhoneCountryCode = (e) => {
    setPhoneCountryCode(e.target.value);
  };
  const handlePhone = (e) => {
    setPhone(e.target.value);
  };

  const handlePromotion = (e) => {
    setPromotion(e.target.value);
  };

  useEffect(() => {
    const getAllPromotion = async () => {
      await getPromotions(Restaurant?.AccessedName).then((res) => {
        if (res?.length > 0) {
          setAllPromotions(res);
        }
      });
    };
    getAllPromotion();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !validator.isEmpty(email, { ignore_whitespace: false }) &&
      !validator.isEmpty(visitDate, { ignore_whitespace: false }) &&
      !validator.isEmpty(visitTime, { ignore_whitespace: false }) &&
      !validator.isEmpty(partySize, { ignore_whitespace: false }) &&
      !validator.isEmpty(firstName, { ignore_whitespace: false }) &&
      !validator.isEmpty(specialRequests, { ignore_whitespace: false }) &&
      !validator.isEmpty(phone, { ignore_whitespace: false }) &&
      !validator.isEmpty(phoneCountryCode, { ignore_whitespace: false }) &&
      validator.isEmail(email) &&
      validator.isDate(visitDate) &&
      validator.isTime(visitTime) &&
      validator.isNumeric(partySize)
    ) {
      await bookingApi(
        visitDate,
        visitTime,
        partySize,
        firstName,
        surname,
        email,
        specialRequests,
        phone,
        phoneCountryCode,
        promotion,
        Restaurant?.AccessedName
      )
        .then((response) => {
          if (response) {
            setLoading_api_response(false);
            if (
              response?.data?.status === 200 ||
              response?.data?.status === 201
            ) {
              setInputError(false);
              setMessage(
                response?.data?.message +
                  ". You will soon receive a confirmation email"
              );
              setSuccess(true);
            } else {
              setInputError(true);
              setMessage(
                response?.response?.data?.message === "Forbidden"
                  ? "An error occurred while processing your request, please try again"
                  : response?.response?.data?.message === "NoAvailability"
                  ? "Sorry we don't have availability for these times"
                  : response?.response?.data?.message ===
                    "A credit card token, or a payment is required in order to secure the booking."
                  ? "A credit card token or payment is required to secure the reservation. Please confirm your reservation on site"
                  : response?.response?.data?.message
              );
              setSuccess(false);
            }
          } else {
            setLoading_api_response(true);
          }
        })
        .catch((e) => {
          setInputError(true);
          setSuccess(false);
          setMessage(e?.response?.data?.message);
          return e?.response?.data?.message;
        });
    } else {
      setInputError(true);
      setSuccess(false);
      setMessage("Please enter all required information correctly.");
    }
  };

  return (
    <Paper container p={2} sx={{ margin: 2, height: 300 }}>
      {loading_api_response && <Loading isLoading={loading_api_response} />}
      {loading ? (
        <Loading isLoading={loading} />
      ) : error ? (
        <Grid
          container
          sx={{ alignItems: "center", justifyContent: "center", width: "100%" }}
        >
          <Typography sx={{ textAlign: "center" }}>
            An error occurred during the display, please try again
          </Typography>
        </Grid>
      ) : (
        <Grid className="detail">
          <Grid sx={{ width: "30%" }}>
            <img
              src={
                Restaurant?.MainImage?.Url ? Restaurant?.MainImage?.Url : image
              }
              alt={"logo"}
              className="image"
            />
          </Grid>
          <Grid sx={{ flexDirection: "column", width: "70%" }} margin={2}>
            <Grid flexDirection={"row"} sx={{ margin: 1 }}>
              <Typography fontSize={16}>
                <strong>Restaurant Name : </strong>
                {Restaurant?.Name}
              </Typography>
            </Grid>
            <Grid flexDirection={"row"} sx={{ margin: 1 }}>
              <Typography fontSize={16}>
                <strong>Location : </strong>
                {Restaurant?.Address?.FullAddress}
              </Typography>
            </Grid>
            <Grid flexDirection={"row"} sx={{ margin: 1 }}>
              <Typography fontSize={16}>
                <strong>Country : </strong>
                {Restaurant?.Address?.Country}
              </Typography>
            </Grid>
            <Grid flexDirection={"row"} sx={{ margin: 1 }}>
              <Typography fontSize={16}>
                <strong>Latitude : </strong>
                {Restaurant?.Address?.Latitude}
              </Typography>
            </Grid>
            <Grid flexDirection={"row"} sx={{ margin: 1 }}>
              <Typography fontSize={16}>
                <strong>Longitude : </strong>
                {Restaurant?.Address?.Longitude}
              </Typography>
            </Grid>
            <Grid flexDirection={"row"} sx={{ margin: 1 }}>
              <Typography fontSize={16}>
                <strong>Restaurant Description : </strong>
                {Restaurant?.Description}
              </Typography>
            </Grid>
            <Grid className="detail-info">
              <Grid>
                <Button
                  onClick={handleClickOpen("paper")}
                  variant="outlined"
                  sx={{
                    margin: 1,
                    color: colors.white,
                    borderColor: colors.black,
                    backgroundColor: colors.black,
                    ":hover": {
                      color: colors.black,
                      borderColor: colors.black,
                      backgroundColor: colors.white,
                    },
                  }}
                >
                  Book now
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <Dialog
            open={open}
            onClose={handleClose}
            scroll={scroll}
            fullWidth={true}
            maxWidth="md"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
          >
            <DialogTitle id="scroll-dialog-title">
              {Restaurant?.Name} Booking
            </DialogTitle>
            <DialogContent
              dividers={scroll === "paper"}
              sx={{ paddingBottom: 2 }}
            >
              <Grid
                container
                textAlign="center"
                justifyContent={"center"}
                direction="column"
              >
                <Typography
                  gutterBottom
                  fontSize={22}
                  fontWeight={800}
                  letterSpacing={0.5}
                >
                  Reservation information
                </Typography>
                <Grid item sx={{ marginBottom: 1, marginTop: 1 }}>
                  {inputError && <Alert severity="error">{message}</Alert>}
                  {success && <Alert severity="success">{message}</Alert>}
                </Grid>
              </Grid>
              <Grid item>
                <Grid item sx={{ marginBottom: 2, marginTop: 2 }}>
                  <TextInput
                    label={"FirstName"}
                    type={"text"}
                    onChange={(e) => {
                      handleFirstName(e);
                    }}
                    value={firstName}
                    name={"firstName"}
                    required={true}
                  />
                </Grid>
                <Grid item sx={{ marginBottom: 2, marginTop: 2 }}>
                  <TextInput
                    label={"SurName"}
                    type={"text"}
                    onChange={(e) => {
                      handleSurname(e);
                    }}
                    value={surname}
                    name={"surname"}
                    required={true}
                  />
                </Grid>
              </Grid>
              <Grid item>
                <Grid item sx={{ marginBottom: 2, marginTop: 2 }}>
                  <TextInput
                    label={"Email"}
                    type={"email"}
                    onChange={(e) => {
                      handleEmail(e);
                    }}
                    value={email}
                    name={"email"}
                    required={true}
                  />
                </Grid>
                <Grid item sx={{ marginBottom: 2, marginTop: 2 }}>
                  <TextInput
                    label={"Phone"}
                    type={"tel"}
                    onChange={(e) => {
                      handlePhone(e);
                    }}
                    value={phone}
                    name={"phone"}
                    required={true}
                  />
                </Grid>
              </Grid>
              <Grid item>
                <Grid item sx={{ marginBottom: 2, marginTop: 2 }}>
                  <TextInput
                    label={"Phone Country Code"}
                    type={"number"}
                    onChange={(e) => {
                      handlePhoneCountryCode(e);
                    }}
                    value={phoneCountryCode}
                    name={"phoneCountryCode"}
                    required={true}
                  />
                </Grid>
                <Grid item sx={{ marginBottom: 2, marginTop: 2 }}>
                  <TextInput
                    label={"Visit Date"}
                    type={"date"}
                    onChange={(e) => {
                      handleVisitDate(e);
                    }}
                    value={visitDate}
                    name={"visitDate"}
                    required={true}
                  />
                </Grid>
              </Grid>
              <Grid item>
                <Grid item sx={{ marginBottom: 2, marginTop: 2 }}>
                  <TextInput
                    label={"Visit Time"}
                    type={"time"}
                    onChange={(e) => {
                      handleVisitTime(e);
                    }}
                    value={visitTime}
                    name={"visitTime"}
                    required={true}
                  />
                </Grid>
                <Grid item sx={{ marginBottom: 2, marginTop: 2 }}>
                  <TextInput
                    label={"Party Size"}
                    type={"number"}
                    maxValue={Restaurant?.MaxCovers}
                    onChange={(e) => {
                      handlePartySize(e);
                    }}
                    value={partySize}
                    name={"partySize"}
                    required={true}
                  />
                </Grid>
                <Grid item sx={{ marginBottom: 2, marginTop: 2 }}>
                  <label fhtmlFor="promotion_id" className={"form-label"}>
                    <strong>Promotions</strong>
                  </label>
                  <select
                    id="promotion_id"
                    class="form-select"
                    aria-label="Default select example"
                    onChange={(e) => {
                      handlePromotion(e);
                    }}
                  >
                    <option selected></option>
                    {allPromotions.length > 0 &&
                      allPromotions.map((value, key) => {
                        return (
                          <option key={key} value={value?.Id}>
                            {value?.Name}
                          </option>
                        );
                      })}
                  </select>
                </Grid>
              </Grid>
              <Grid item>
                <TextInput
                  label={"Special Requests"}
                  type={"text"}
                  onChange={(e) => {
                    handleSpecialRequests(e);
                  }}
                  value={specialRequests}
                  name={"specialRequests"}
                  required={true}
                />
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={handleClose}
                variant="outlined"
                sx={{
                  margin: 1,
                  color: colors.black,
                  borderColor: colors.black,
                  backgroundColor: colors.white,
                  ":hover": {
                    color: colors.white,
                    borderColor: colors.black,
                    backgroundColor: colors.black,
                  },
                }}
              >
                Cancel
              </Button>
              <Button
                onClick={(e) => handleSubmit(e)}
                variant="outlined"
                sx={{
                  margin: 1,
                  color: colors.white,
                  borderColor: colors.black,
                  backgroundColor: colors.black,
                  ":hover": {
                    color: colors.black,
                    borderColor: colors.black,
                    backgroundColor: colors.white,
                  },
                }}
              >
                Submit
              </Button>
            </DialogActions>
          </Dialog>
        </Grid>
      )}
    </Paper>
  );
}
